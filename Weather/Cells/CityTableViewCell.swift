import UIKit
import Kingfisher

class CityTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var isLocationImage: UIImageView!
        
    var isLocation = false

    func configure(weather: Weather, isLocation: Bool) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
                        
            let imgURL = URL(string: "https:" + weather.current.condition.icon)
            
            self.cityNameLabel.adjustsFontSizeToFitWidth = true
            self.cityNameLabel.adjustsFontForContentSizeCategory = true
            self.cityNameLabel.text = weather.location.name
            self.tempLabel.text = "\(Int(weather.current.temp_c))°"
            self.iconView.kf.setImage(with: imgURL)
            
            if isLocation {
                self.isLocation = isLocation
                self.isLocationImage.isHidden = false
            } else {
                self.isLocationImage.isHidden = true
            }
        }
    }
}

