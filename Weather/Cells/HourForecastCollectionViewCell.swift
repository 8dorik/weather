import UIKit

class HourForecastCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    func configure(with temp: Double, image: String, hour: String) {
        imageView.kf.setImage(with: URL(string: image))
        
        let time = String(hour.suffix(5))
        DispatchQueue.main.async { [weak self] in
            self?.tempLabel.text = "\(Int(temp))°"
            self?.hourLabel.text = time
        }
    }
}
