import UIKit

class DayForecastTableViewCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var dayNameLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    
    func configure(minTemp: Double, maxTemp: Double, image: String, day: String) {        
        iconView.kf.setImage(with: URL(string: image))
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let date = dateFormatter.date(from: day) else { return }
        
        dateFormatter.dateFormat = "EEEE"
        let day = dateFormatter.string(from: date)
        
        DispatchQueue.main.async { [weak self] in
            self?.dayNameLabel.text = day
            self?.minTempLabel.text = "\(Int(minTemp))°"
            self?.maxTempLabel.text = "\(Int(maxTemp))°"
        }
    }
}
