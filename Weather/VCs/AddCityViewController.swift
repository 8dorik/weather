import UIKit

protocol AddCityViewControllerDelegate: AnyObject {
    func didAddNewCity()
}

class AddCityViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addCityLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    weak var delegate: AddCityViewControllerDelegate?
    private let viewModel = ACVCViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension AddCityViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.autocomplete.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddCityTableViewCell", for: indexPath) as? AddCityTableViewCell else { return UITableViewCell() }
        cell.textLabel?.text = viewModel.autocomplete[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.autocomplete.count - 1 >= indexPath.row{
            viewModel.isCityValid(index: indexPath.row, completion: { exists in
                if !exists {
                    self.showAlert(title: L10n.invalidCity, message: L10n.tryAnotherOne, defaultAction: nil)
                } else {
                    Manager.shared.cities.append(self.viewModel.autocomplete[indexPath.row])
                    self.delegate?.didAddNewCity()
                    self.dismiss(animated: true, completion: nil)
                }
                
            })
            
            
            
            
        }
    }
    
    private func localize() {
        addCityLabel.text = L10n.addCity
        cancelButton.setTitle(L10n.cancel, for: .normal)
    }
}

extension AddCityViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.autocompleteCities(with: searchText) { [weak self] error in
            if error != nil {
                self?.showAlert(title: L10n.notFound, message: L10n.noCitiesFound, defaultAction: nil)
            }
            self?.tableView.reloadData()
        }
    }
}
