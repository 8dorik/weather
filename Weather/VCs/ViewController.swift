import Kingfisher
import UIKit
import CoreLocation
import DropDown
import MapKit

class ViewController: UIViewController {
    
    @IBOutlet weak var dailyForecastLabel: UILabel!
    @IBOutlet weak var hourForecastLabel: UILabel!
    @IBOutlet weak var localTimeLabel: UILabel!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var conditionImage: UIImageView!
    @IBOutlet weak var weatherConditionLabel: UILabel!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var cityName: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dayCounter: UISegmentedControl!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var insideView: UIView!
    
    private let viewModel = VCViewModel()
    private let dropDown: DropDown = {
        let dropDown = DropDown()
        dropDown.cornerRadius = 15
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        return dropDown
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupScreen()
        bind()
    }
    
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        //        scrollView.contentSize = CGSize(width: view.frame.width, height: mapView.frame.origin.y + mapView.frame.height)
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        setBackground(viewModel.dailyColors(weather: viewModel.currentWeather))
        scrollView.contentSize = CGSize(width: view.frame.width, height: mapView.frame.origin.y + mapView.frame.height)
    }
    
    @IBAction func cityNameButtonPressed(_ sender: UIButton) {
        cityName.imageView?.transform = CGAffineTransform(rotationAngle: .pi)
        UIView.animate(withDuration: 0.3) {
            self.cityName.imageView?.transform = CGAffineTransform.identity
        }
        dropDown.show()
    }
    
    @IBAction func changeCityButtonPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "TVC") as? TableViewController else { return }
        controller.delegate = self
        controller.selectedCity = viewModel.city.value
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func updateButtonPressed(_ sender: UIButton) {
        updateData()
    }
    
    @IBAction func dayCounterSwitched(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            viewModel.days = 1
        case 1:
            viewModel.days = 3
        default:
            break
        }
        tableView.reloadData()
    }
    
    private func setupScreen() {
        localize()
        configureRefreshControl()
        blurView.clipsToBounds = true
        blurView.layer.cornerRadius = 10
        activityIndicator.hidesWhenStopped = true
        tableView.isUserInteractionEnabled = false
        tableView.rowHeight = 44
        activityIndicator.startAnimating()
        activityIndicator.superview?.layer.cornerRadius = 10
        mapView.layer.cornerRadius = 20
        mapView.isScrollEnabled = true
        mapView.isZoomEnabled = true
        mapView.isUserInteractionEnabled = true
        initialDataLoad()
    }
    
    private func setupDropDown() {
        dropDown.anchorView = cityName
        dropDown.bottomOffset = CGPoint(x: 0, y: cityName.bounds.height)
        dropDown.dataSource = Manager.shared.cities
        dropDown.cellConfiguration = { [weak self] (index, item) in
            if item == self?.viewModel.city.value {
                self?.dropDown.selectRow(at: index)
            }
            
            return item
        }
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let self = self else { return }
            
            self.dropDown.deselectRows(at: Set(0...Manager.shared.cities.count))
            self.loadCurrentWeather(cityName: item)
        }
        
    }
    
    private func initialDataLoad() {
        viewModel.loadLocation { [weak self] cityName, error in
            guard let self = self else { return }
            if error == true {
                self.showAlert(defaultTitle: L10n.openSettings, cancelTitle: L10n.cancel, title: L10n.locationError, message: L10n.turnOnLocation) {
                    guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
                    UIApplication.shared.open(url,
                                              options: [:],
                                              completionHandler: nil)
                } cancelAction: { [weak self] in
                    guard let self = self else { return }
                    self.showAlert(title: L10n.autoIp, message: L10n.cityAutoIp) {
                        self.viewModel.location = "auto:ip"
                        self.loadCurrentWeather(cityName: "auto:ip")
                    }
                }
            } else {
                self.loadCurrentWeather(cityName: cityName) {
                    self.setupDropDown()
                }
            }
        }
    }
    
    private func loadCurrentWeather(cityName: String, completion: (() -> ())? = nil ) {
        self.viewModel.loadData(cityName: cityName) { [weak self] didRecieve in
            guard let self = self else { return }
            
            guard didRecieve else {
                self.showAlert(title: L10n.error, message: L10n.noData, defaultAction: nil)
                return
            }
            self.mapView.centerToLocation(self.viewModel.mapLocation ?? CLLocation(latitude: 50.45, longitude: 30.52))
            self.mapView.addAnnotation(self.viewModel.mapLocation ?? CLLocation(latitude: 50.45, longitude: 30.52),
                                       country: self.viewModel.currentWeather?.location.country,
                                       city: self.viewModel.currentWeather?.location.name)
            self.activityIndicator.stopAnimating()
            self.blurView.isHidden = true
            completion?()
            self.setBackground(self.viewModel.dailyColors(weather: self.viewModel.currentWeather))
            self.tableView.reloadData()
            self.collectionView.reloadData()
            self.setupDropDown()
            self.dropDown.reloadAllComponents()
        }
    }
    
    
    private func setBackground(_ colors: [CGColor]) {
        self.backgroundView.layer.sublayers?.removeAll()
        self.backgroundView.addGradient(colors: colors)
    }
    
    
    private func bind() {
        viewModel.city.bind { title in
            DispatchQueue.main.async { [weak self] in
                self?.title = title
                self?.cityName.setTitle(title, for: .normal)
            }
        }
        viewModel.time.bind { text in
            DispatchQueue.main.async { [weak self] in
                self?.timeLabel.text = text
            }
        }
        viewModel.temperature.bind { text in
            DispatchQueue.main.async { [weak self] in
                self?.tempLabel.text = text
            }
        }
        viewModel.condition.bind { text in
            DispatchQueue.main.async { [weak self] in
                self?.weatherConditionLabel.text = text
            }
        }
        viewModel.conditionImageName.bind { string in
            DispatchQueue.main.async { [weak self] in
                let url = URL(string: "https:" + string)
                self?.conditionImage.kf.setImage(with: url)
            }
        }
    }
    
    private func updateData(showView: Bool = true) {
        if showView {
            self.activityIndicator.startAnimating()
            self.blurView.isHidden = false
        }
        self.disableButtons()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
            guard let self = self else { return }
            self.loadCurrentWeather(cityName: self.viewModel.city.value) {
                self.enableButtons()
            }
        }
    }
    
    private func enableButtons() {
        cityName.isEnabled = true
        updateButton.isEnabled = true
        dayCounter.isEnabled = true
        addButton.isEnabled = true
        scrollView.refreshControl?.endRefreshing()
    }
    
    private func disableButtons() {
        cityName.isEnabled = false
        updateButton.isEnabled = false
        dayCounter.isEnabled = false
        addButton.isEnabled = false
    }
    
    private func localize() {
        updateButton.setTitle(L10n.update, for: .normal)
        dayCounter.setTitle(L10n._1d, forSegmentAt: 0)
        dayCounter.setTitle(L10n._3d, forSegmentAt: 1)
        localTimeLabel.text = L10n.localTime
        changeButton.setTitle(L10n.change, for: .normal)
        hourForecastLabel.text = L10n.hourForecast
        dailyForecastLabel.text = L10n.dailyForecast
    }
    
    func configureRefreshControl() {
        scrollView.refreshControl = UIRefreshControl()
        scrollView.refreshControl?.largeContentTitle = "Pull to refresh"
        scrollView.refreshControl?.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
    }
    
    @objc func handleRefreshControl() {
        updateData(showView: false)
    }
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.days
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DayForecastTableViewCell", for: indexPath) as? DayForecastTableViewCell else { return UITableViewCell() }
        guard let weather = viewModel.currentWeather else {
            return UITableViewCell()
        }
        cell.configure(minTemp: weather.forecast.forecastday[indexPath.row].day.mintemp_c,
                       maxTemp: weather.forecast.forecastday[indexPath.row].day.maxtemp_c,
                       image: "https:" + weather.forecast.forecastday[indexPath.row].day.condition.icon,
                       day: weather.forecast.forecastday[indexPath.row].date)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let hours = viewModel.currentWeather?.forecast.forecastday.first?.hour else { return 0 }
        return viewModel.filterHourForecast(array: hours).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let hours = viewModel.currentWeather?.forecast.forecastday.first?.hour else { return UICollectionViewCell() }
        let filteredHours = viewModel.filterHourForecast(array: hours)
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourForecastCollectionViewCell", for: indexPath) as? HourForecastCollectionViewCell else { return UICollectionViewCell() }
        
        cell.configure(with: filteredHours[indexPath.item].temp_c,
                       image: "https:" + filteredHours[indexPath.item].condition.icon,
                       hour: filteredHours[indexPath.item].time)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.width - 80) / 4
        return CGSize(width: width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

extension ViewController: TVCDelegate, MKMapViewDelegate {
    func didSelectCity(withName name: String) {
        viewModel.city.value = name
        updateData()
        dropDown.reloadAllComponents()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
        annotationView.canShowCallout = true
        return annotationView
    }
    
}

private extension MKMapView {
    func centerToLocation(_ location: CLLocation, regionRadius: CLLocationDistance = 10000) {
        let coordinateRegion = MKCoordinateRegion(
            center: location.coordinate,
            latitudinalMeters: regionRadius,
            longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: true)
    }
    
    func addAnnotation(_ location: CLLocation, country: String?, city: String?) {
        self.removeAnnotations(self.annotations)
        let point = MKPointAnnotation()
        if let city = city {
            point.title = city
            if let country = country {
                point.title? += ", \(country)"
            }
        }
        point.coordinate = location.coordinate
        self.addAnnotation(point)
    }
}

