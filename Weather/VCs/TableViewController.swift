import UIKit

protocol TVCDelegate: AnyObject {
    func didSelectCity(withName name: String)
}

class TableViewController: UIViewController {
    
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var citiesLabel: UILabel!
    
    private let viewModel = TVCViewModel()
    
    weak var delegate: TVCDelegate?
    
    var selectedCity: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
        blurView.isHidden = false
        indicator.hidesWhenStopped = true
        indicator.startAnimating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animateTableView()
    }
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddCityViewController") as? AddCityViewController else { return }
        controller.delegate = self
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        if let selectedCity = selectedCity,
           let city = Manager.shared.cities.first,
           !Manager.shared.cities.contains(selectedCity) {
            delegate?.didSelectCity(withName: Manager.shared.location ?? city)
        } else {
            guard let  selectedCity = selectedCity else { return }
            delegate?.didSelectCity(withName: selectedCity)
        }
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func changeButtonPressed(_ sender: UIButton) {
        showEditing(sender)
    }
    
    private func showEditing(_ sender: UIButton)
    {
        if(self.tableView.isEditing == true) {
            tableView.setEditing(false, animated: true)
            sender.isSelected = false
        } else {
            tableView.setEditing(true, animated: true)
            sender.isSelected = true
        }
    }
    
    private func animateTableView() {
        tableView.reloadData()
        let cells = tableView.visibleCells
        var delay = 0.0
        
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: view.frame.width, y: 0)
            UIView.animate(withDuration: 1,
                           delay: delay * 0.05,
                           usingSpringWithDamping: 0.8,
                           initialSpringVelocity: 0,
                           options: .curveEaseInOut,
                           animations: {
                            cell.transform = CGAffineTransform.identity
                           }, completion: nil)
            delay += 1
        }
    }
    
    private func localize() {
        backButton.setTitle(L10n.back, for: .normal)
        changeButton.setTitle(L10n.change, for: .normal)
        changeButton.setTitle(L10n.done, for: .selected)
        citiesLabel.text = L10n.cities
    }
    
}

extension TableViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Manager.shared.cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CityTableViewCell", for: indexPath) as? CityTableViewCell else { return UITableViewCell() }
        
        var isLocation = false
        if Manager.shared.cities[indexPath.row] == Manager.shared.location {
            isLocation = true
        }
        
        
        viewModel.loadData(forCity: Manager.shared.cities[indexPath.row]) { [weak self] weather in
            guard let self = self else { return }
            cell.configure(weather: weather, isLocation: isLocation)
            
            if indexPath.row == Manager.shared.cities.count - 1 {
                self.blurView.isHidden = true
                self.indicator.stopAnimating()
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectCity(withName: Manager.shared.cities[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let cell = tableView.cellForRow(at: indexPath) as? CityTableViewCell else { return }
            if cell.isLocation {
                showAlert(title: "This is your location", message: "You can`t remove this city", defaultAction: nil)
            } else {
                Manager.shared.cities.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        }
    }
    
}

extension TableViewController: AddCityViewControllerDelegate {
    func didAddNewCity() {
        tableView.reloadData()
    }
}

