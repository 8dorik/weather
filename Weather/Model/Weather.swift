import Foundation


struct Weather: Decodable {
    let location: Location
    let current: Current
    let forecast: Forecast
}

struct Forecast: Decodable {
    let forecastday: [Forecastday]
}

struct Forecastday: Decodable {
    let date: String
    let day: Day
    let astro: Astro
    let hour: [Hour]
}

struct Astro: Decodable {
    let sunrise: String
    let sunset: String
}

struct Day: Decodable {
    let maxtemp_c: Double
    let mintemp_c: Double
    let condition: Condition
}

struct Hour: Decodable {
    let time: String
    let temp_c: Double
    let condition: Condition
}

struct Location: Decodable {
    let name: String
    let country: String
    let localtime: String
    let lat: Double
    let lon: Double
}

struct Current: Decodable {
    let temp_c: Double
    let wind_kph: Double
    let wind_dir: String
    let pressure_mb: Double
    let condition: Condition
}

struct Condition: Decodable {
    let text: String
    let icon: String
}
