import Foundation

enum DecodeKeys: String {
    case cities
    case defaultCity
    case settings
}
