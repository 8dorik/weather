import Foundation
import UIKit

class Manager {

    static let shared = Manager()
    
    let weatherURL = "http://api.weatherapi.com/v1/forecast.json"
    
    var cities: [String] {
        get {
            guard let cities = UserDefaults.standard.value(forKey: DecodeKeys.cities.rawValue) as? [String] else { return [String]() }
            let set = Array(Set(cities)).sorted()
            return set
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: DecodeKeys.cities.rawValue)
        }
    }
    
    var location: String?
    
    private init() {}
    
    func getInfo<T: Decodable>(fromURL url: String,
                               into type: T.Type,
                               withParameters parameters: [String : String],
                               completion: @escaping (T?, Error?) -> ()){
        
        guard let url = getURL(url, withParameters: parameters) else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if error == nil, let data = data {
                    let object = self.parceJSON(data: data, type: type)
                    completion(object, error)
                } else {
                    completion(nil, error)
                }
            }
        }
        
        task.resume()
    }
    
    func getURL(_ url: String, withParameters parameters: [String : String]) -> URL? {
        var components = URLComponents(string: url)
        
        components?.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        
        return components?.url
        
    }
    
    
    private func parceJSON<T: Decodable>(data: Data, type: T.Type) -> T?{
        let object: T?
        
        do {
            object = try JSONDecoder().decode(type, from: data)
            return object
        } catch {
            print(error)
        }
        
        return nil
    }

    
}
