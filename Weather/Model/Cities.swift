import Foundation

struct City: Decodable {
    let name: String
    let lat: Double
    let lon: Double
}

