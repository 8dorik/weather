import Foundation
import UIKit


extension UIView {
    func addGradient(colors: [CGColor]) {
            let gradient = CAGradientLayer()
            
            gradient.colors = colors
            gradient.opacity = 0.8
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        
            gradient.frame = self.bounds
            self.layer.insertSublayer(gradient, at: 0)
        }
}
