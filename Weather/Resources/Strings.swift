// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  /// 1d
  internal static let _1d = L10n.tr("Localizable", "1d")
  /// 3d
  internal static let _3d = L10n.tr("Localizable", "3d")
  /// Add city
  internal static let addCity = L10n.tr("Localizable", "add_city")
  /// Auto IP
  internal static let autoIp = L10n.tr("Localizable", "auto_ip")
  /// Back
  internal static let back = L10n.tr("Localizable", "back")
  /// Cancel
  internal static let cancel = L10n.tr("Localizable", "cancel")
  /// Change
  internal static let change = L10n.tr("Localizable", "change")
  /// Cities
  internal static let cities = L10n.tr("Localizable", "cities")
  /// City will be chosen based on your IP
  internal static let cityAutoIp = L10n.tr("Localizable", "city_auto_ip")
  /// Daily forecast
  internal static let dailyForecast = L10n.tr("Localizable", "daily_forecast")
  /// Done
  internal static let done = L10n.tr("Localizable", "done")
  /// Error
  internal static let error = L10n.tr("Localizable", "error")
  /// Hour forecast
  internal static let hourForecast = L10n.tr("Localizable", "hour_forecast")
  /// Invalid city
  internal static let invalidCity = L10n.tr("Localizable", "invalid_city")
  /// Local time:
  internal static let localTime = L10n.tr("Localizable", "local_time")
  /// Location Error
  internal static let locationError = L10n.tr("Localizable", "location_error")
  /// No cities with such name found
  internal static let noCitiesFound = L10n.tr("Localizable", "no_cities_found")
  /// Did not recieve data from server
  internal static let noData = L10n.tr("Localizable", "no_data")
  /// Not Found
  internal static let notFound = L10n.tr("Localizable", "not_found")
  /// Open settings
  internal static let openSettings = L10n.tr("Localizable", "open_settings")
  /// This is your location
  internal static let thisIsYourLocation = L10n.tr("Localizable", "this_is_your_location")
  /// Try to choose another one
  internal static let tryAnotherOne = L10n.tr("Localizable", "try_another_one")
  /// Turn on location in your settings
  internal static let turnOnLocation = L10n.tr("Localizable", "turn_on_location")
  /// Update
  internal static let update = L10n.tr("Localizable", "update")
  /// You can`t remove this city
  internal static let youCantRemoveThisCity = L10n.tr("Localizable", "you_cant_remove_this_city")
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
