import Kingfisher
import UIKit
import CoreLocation

class VCViewModel: NSObject {
    
    var temperature = Bindable<String>("")
    var city = Bindable<String>("")
    var time = Bindable<String>("")
    var condition = Bindable<String>("")
    var conditionImageName = Bindable<String>("")
    var currentWeather: Weather?
    var didGetLocation: ((String, Bool) -> ())?
    var location: String?
    var mapLocation: CLLocation?
    var days = 3
    var timer: Timer?
    
    var isLocation = false
    var locationManager = CLLocationManager()
    
    var urlParameters = [
        "key" : "7e4b4457e42c441283c184324211108",
        "days" : "5",
        "q" : "auto:ip"
    ]

    
    func loadData(cityName: String, completion: ((Bool) -> ())? = nil) {
        urlParameters["q"] = cityName
        var handler : ((String) -> ())?
        if cityName == location {
            handler = { text in
                Manager.shared.location = text
                Manager.shared.cities.append(text)
            }
        }
        Manager.shared.getInfo(fromURL: Manager.shared.weatherURL,
                               into: Weather.self,
                               withParameters: urlParameters) { [weak self] weather, error in
            guard let weather = weather,
                  let self = self else {
                completion?(false)
                return
            }
            self.currentWeather = weather
            self.conditionImageName.value = weather.current.condition.icon
            self.condition.value = weather.current.condition.text
            self.temperature.value = "\(Int(weather.current.temp_c))°"
            self.city.value = weather.location.name
            self.mapLocation = CLLocation(latitude: weather.location.lat, longitude: weather.location.lon)
            handler?(self.city.value)
            self.time.value = self.convertDate(weather.location.localtime)
            completion?(true)
        }
    }
    
    func isDark(time: String, sunrise: String, sunset: String) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        guard let date = dateFormatter.date(from: time) else { return false }
        
        let prefix = time.prefix(11)
        
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        guard let sunriseTime = dateFormatter.date(from: prefix + sunrise) else { return false }
        guard let sunsetTime = dateFormatter.date(from: prefix + sunset) else { return false }
        
        if date.timeIntervalSince(sunriseTime) < 0 || date.timeIntervalSince(sunsetTime) > 0 {
            return true
        }
        return false
    }
    
    private func convertDate(_ string: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        guard let date = dateFormatter.date(from: string) else { return "" }
        dateFormatter.dateFormat = "MMM d, HH:mm"
        let time = dateFormatter.string(from: date)
        return time
    }
    
    func dailyColors(weather: Weather?) -> [CGColor] {
        guard let weather = weather,
              let sunrise = weather.forecast.forecastday.first?.astro.sunrise,
              let sunset = weather.forecast.forecastday.first?.astro.sunset else {
            return [UIColor.blue.cgColor, UIColor.systemTeal.cgColor, UIColor.white.cgColor]
        }
        
        let time = weather.location.localtime
        
        if !isDark(time: time, sunrise: sunrise, sunset: sunset){
            return [UIColor.blue.cgColor, UIColor.systemTeal.cgColor, UIColor.white.cgColor]
        } else {
            return [UIColor.darkGray.cgColor, UIColor.gray.cgColor, UIColor.lightGray.cgColor]
        }
    }
    func filterHourForecast(array: [Hour]) -> [Hour] {
        let filtered = array.filter({
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
            if let time = currentWeather?.location.localtime {
            guard let current = dateFormatter.date(from: time) else { return false }
            guard let forecast = dateFormatter.date(from: $0.time) else { return false }
            if current.timeIntervalSince(forecast) <= 3600 {
                    return true
                }
            }
            return false
        })
        return filtered
    }
}

extension VCViewModel: CLLocationManagerDelegate {
    func loadLocation(completion: ((String, Bool) -> ())? = nil) {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: { [weak self] _ in
            if self?.location == nil {
                completion?("", true)
            }
        })
        didGetLocation = completion
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location error: ", error)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if location == nil {
            isLocation = true
            guard let location = manager.location?.coordinate else { return }
            self.location = "\(location.latitude),\(location.longitude)"
            self.mapLocation = manager.location
            didGetLocation?("\(location.latitude),\(location.longitude)", false)
            manager.stopUpdatingLocation()
        }
    }
}


