import UIKit

class TVCViewModel {
    
    let weatherURL = "http://api.weatherapi.com/v1/forecast.json"
    var weatherParameters =
        [
            "key" : "7e4b4457e42c441283c184324211108",
            "days" : "3"
        ]
    
    func loadData(forCity city: String, completion: @escaping (Weather) -> ()) {
        weatherParameters["q"] = city
        Manager.shared.getInfo(fromURL: weatherURL,
                               into: Weather.self,
                               withParameters: weatherParameters) { weather, error in
            guard let weather = weather else { return }
            completion(weather)
        }
    }
}
