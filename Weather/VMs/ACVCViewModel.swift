import UIKit

class ACVCViewModel {
    
    let weatherURL = "http://api.weatherapi.com/v1/forecast.json"
    var weatherParameters =
        [
            "key" : "7e4b4457e42c441283c184324211108",
            "days" : "5",
            "q" : ""
        ]
    
    let citiesURL = "http://api.weatherapi.com/v1/search.json"
    var parameters = [
        "key" : "7e4b4457e42c441283c184324211108",
        "q" : "",
        
    ]
    var autocomplete = [String]()
    
    func isCityValid(index: Int, completion: @escaping (Bool) -> () ) {
        weatherParameters["q"] = autocomplete[index]
        
        Manager.shared.getInfo(fromURL: weatherURL, into: Weather.self, withParameters: weatherParameters) { object, error in
            DispatchQueue.main.async {
                if object == nil {
                    completion(false)
                } else {
                    completion(true)
                }
            }
        }
    }
    
    func autocompleteCities(with name: String, completion: @escaping (Error?) -> ()) {
        autocomplete.removeAll()
        parameters["q"] = name
        Manager.shared.getInfo(fromURL: citiesURL, into: [City].self, withParameters: parameters) { [weak self] cities, error in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if let cities = cities {
                    for city in cities {
                        self.autocomplete.append(self.cityNameOnly(city.name))
                    }
                } else {
                    self.autocomplete.removeAll()
                }
                completion(error)
            }
        }
    }
    
    func cityNameOnly(_ name: String) -> String {
        var cityName = ""
        
        for char in name {
            if char != "," {
                cityName += String(char)
            } else  {
                break
            }
        }
        
        return cityName
    }
}
